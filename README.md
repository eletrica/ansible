# Ansible

Ansible project to configure machines.

## Requisites

- ansible
- python3-psutil

## Usage

```
ansible-playbook -K main.yaml
```

The `-K` argument it's required to run some commmands in sudo mode.
